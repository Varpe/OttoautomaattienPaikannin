<!DOCTYPE html>
<html>
	<head>
		<title>Ottomaattien Etsijä</title>
		 <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="otto.css">
<link rel="shortcut icon" type="image/png" href="ottofavikoni.png"/>

		
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script src="http://maps.googleapis.com/maps/api/js?libraries=geometry"></script>
<script src="map.js"></script>
	</head>
<body>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<div id="topbar" class="container">Otto-automaattipaikannin
	<button type="button" id="oikea" class="btn btn-primary" onclick="getLocation()">Paikanna</button>
	<div class="row">
		<div class="col-sm-3">
					<input type="range" min="0" max="5" step="1" id="maxdistance" onchange="showValue()"> 
				<p id="muuttuva">Etaisyys, jolta haetaan:</p>
		</div>
		<div id="geoCode" class="col-sm-3">
      <input id="address" type="textbox" value="" onkeypress="enter(event)">
      <button id="hakuNappi" type="button" class="btn btn-primary" onclick="codeAddress()">Etsi osoitteella </button>
    </div>
	</div>		
	</div>
<div id="googleMap" class="container"></div>
<div id="bottombar"><h2>Lahimmat ottomaatit:</h2></div>
	<script>
		createMap();
		</script>
	<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ottomaatit";

// Luodaan sql-yhteys
$conn = new mysqli($servername, $username, $password, $dbname);
// Testataan sql-yhteyttä
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//Tehdään kysely
$sql = "SELECT Numero, Kohteen_osoite, Postinumero, Postitoimipaikka, Koordinaatti_LAT, Koordinaatti_LON, Sijaintipaikka FROM automaatit2";
$result = $conn->query($sql);
	

if ($result->num_rows > 0) {
    // Käydään tulokset läpi ja yhdistetään ne javascript muuttujiin
    while($row = $result->fetch_assoc()) {
		$lat = $row["Koordinaatti_LAT"];
		$lon = $row["Koordinaatti_LON"];
		$osote = $row["Kohteen_osoite"];
		$pstnro = $row["Postinumero"];
		$id = $row["Numero"];
		$sijainti = $row["Sijaintipaikka"];
		$kaupunki = $row["Postitoimipaikka"];
		?>
		<script>
		var id = "<?php echo $id ?>" ;
		var lati = <?php echo $lat ?> ;
		var long = <?php echo $lon ?> ;
		var osoite = "<?php echo $osote ?>" ;
		var pstnro = "<?php echo $pstnro ?>" ;
		var kaupunki = "<?php echo $kaupunki ?>" ;
		var sijainti = "<?php echo $sijainti ?>" ;
		placeMarker(lati, long, osoite, pstnro, id, kaupunki, sijainti);
	</script>
     <?php	
    }
} else {
    echo "0 results";
}
//Suljetaan yhteys
$conn->close();
 ?>
</body>
</html>
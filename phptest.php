<!DOCTYPE html>
<html>
	<head>
		 <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="otto.css">
		
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script src="map.js"></script>
	</head>
<body>
<div id="topbar" class="container">Otto-automaattipaikannin
	
		<button type="button" id="oikea" name="go" class="btn btn-primary" onclick="sqlQuery()">Paikanna</button>
	
	</div>
<div id="googleMap" class="container"></div>
<div id="bottombar"><h2>Lahimmat ottomaatit:</h2></div>
<?php ?>
	<script>
		createMap();
		</script>
	<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ottomaatit";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT Numero, Kohteen_osoite, Postinumero, Postitoimipaikka,LAT, LON,  FROM automaatit";
$result = $conn->query($sql);
	

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
		$lat = $row["LAT"];
		$lon = $row["LON"];
		$osote = $row["Kohteen_osoite"];
		$pstnro = $row["Postinumero"];
		$id = $row["Numero"]
		?>
		<script>
		var id = "<?php echo $id ?>" ;
		var lati = <?php echo $lat ?> ;
		var long = <?php echo $lon ?> ;
		var autolatlng = createLatLng(lati, long);
		var osoite = "<?php echo $osote ?>" ;
		var pstnro = "<?php echo $pstnro ?>" ;
		placeMarker(autolatlng, osoite, pstnro, id);
	</script>
     <?php	
    }
} else {
    echo "0 results";
}
$conn->close();
 	
</body>
</html>
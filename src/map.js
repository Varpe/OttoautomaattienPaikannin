var map;
/**
 * Keskittää kartan kyseisiin koordinaatteihin (Helsinki)
 */
var myCenter = new google.maps.LatLng(60.168718, 24.941095);

/**
 * Ottomaattien tiedot kerätään collection taulukkoon
*/
var collection = [];

/**
 * Testausvaiheessa käytetty muuttuja
*/
var index = 0;
/**
 * Käyttäjän paikkatiedot kerätään userMarkers taulukkoon
*/
var userMarkers = [];

/**
 * Muuntaa käyttäjän koordinaatit LatLong objektiksi, jota 
 * googlemaps käyttää
 */
function getLocationPosition(position) {
    var userPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    calcDistance(userPosition);
}

/** Funktio jonka avulla haku voidaan tehdä enter-näppäintä * * painamalla
*/
function enter(event) {
	if (event.which == 13 || event.keyCode == 13) {
		codeAddress();
		scroll(0,0);
		return false;
	}
	return true;
}

/**
 * Funktio joka paikantaa käyttäjän sijainnin
*/
function getLocation() {
	userMarkerClear();
	scroll(0,0);
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(setUserMarker);
        navigator.geolocation.getCurrentPosition(getLocationPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

/** Funktio joka päivittää etäisyysvalitsimen arvon ja *näyttää  sen sivustolla
*/
function showValue() {
    var maxdist = document.getElementById("maxdistance").value;
    var para = document.getElementById("muuttuva");
    var infoText = "Etaisyys, jolta haetaan: " + maxdist + " km";
    para.innerHTML = infoText;
}

/**
 * Funktio joka alustaa karttamerkinnät
 */
function resetMap() {
    for(var k = 0; k < collection.length; k++) {
        setMap(null, collection[k]);
    }
}

/**
* Laskee käyttäjän etäisyyden kaikkiin otto-automaatteihin
*Mikäli etäisyys on pienempi kuin haluttu hakuetäisyys -> *lisätään lahinViisi taulukkoon
*joka järjestetään lähimmästä kauimpaan ja esitetään sivun alalaidassa (sortandPlot)
*/
function calcDistance(a) {
    var maxdist = document.getElementById("maxdistance").value;
    var maxm = maxdist * 1000;
    resetMap();
    var lahinViisi = [];
    for(var i = 0; i < collection.length; i++) {
        var b = collection[i].position;
        var distance = google.maps.geometry.spherical.computeDistanceBetween(a, b);
        collection[i].distance = distance;
        if(collection[i].distance < maxm) {
            lahinViisi.push(collection[i]);
        }
    }
    var divari = document.getElementById("bottombar");
    clearDiv(divari);
    sortandPlot(lahinViisi);
}

/**
 * Funktio joka lisää tiedot automaateista nettisivulle
*/
function addInfo(osoite, pstnro, distance) {
    var divari = document.getElementById("bottombar");
    var distkm = (distance / 1000);
    distkm = distkm.toFixed(2);
    var para = document.createElement("p");
    var infoteksti = document.createTextNode(osoite + ", " + pstnro + ", " + distkm + " km");
    para.appendChild(infoteksti);
    divari.appendChild(para);
}

/**
 * Funktio joka tyhjentää hakutulokset
*/
function clearDiv(div) {
    div.innerHTML = " ";
    div.innerHTML = "<h2>Lahimmat ottomaatit:</h2>";
}

/**
 * Järjestää automaatit etäisyyden mukaan ja lisää *karttamerkit sekä suorittaa addInfo funktion (ks. ylempää)
 */
function sortandPlot(array) {
    array.sort(sort_by('distance', false, parseInt));
    for(var i = 0; i < array.length; i++) {
        addInfo(array[i].osoite, array[i].pstnro, array[i].distance);
        setMap(map, array[i]);
    }
}

/**
 * Automaattien järjestämiseen hyödynnettävä funktio
*/
var sort_by = function(field, reverse, primer) {
    var key = primer ? function(x) {
            return primer(x[field]);
        } : function(x) {
            return x[field];
        };
    reverse = !reverse ? 1 : -1;
    return function(a, b) {
        return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
    };
};

/**
 * Asettaa käyttäjän paikan kartalle ja lisää siihen -onclick tapahtuman
 */
function setUserMarker(position) {
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
    });
    marker.setMap(map);
    var infowindow = new google.maps.InfoWindow({
        content: "Olet tassa."
    });
    userMarkers.push(marker);
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
    });
    map.setCenter(marker.getPosition());
}

/**
 * Luo karttanäkymän sivulle kun se ladataan ensimmäistä kertaa
*/
function createMap() {
    var mapProp = {
        center: myCenter,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        zoomControl: true
    };
    map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    google.maps.event.addDomListener(window, 'load', initialize);
}

/**
 * Funktio jokaMahdollistaa karttamerkkien piilottamisen (uudet haut)
 */
function setMap(map, marker) {
    marker.setMap(map);
}

/**
 * Funktio joka poistaa käyttäjän karttamerkinnän
*/
function userMarkerClear() {
    if(userMarkers.length > 0) {
        for(var l = 0; l < userMarkers.length; l++) {
            setMap(null, userMarkers[l]);
        }
    }
}



geocoder = new google.maps.Geocoder();
/**
 * Funktio käyttäjän paikantamiseen osoitetiedon avulla
 */
function codeAddress() {  
    resetMap();
	userMarkerClear();
    var address = document.getElementById("address").value;
    geocoder.geocode({
        'address': address
    }, function(results, status) {
        if(status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            var userPos = results[0].geometry.location;
            console.log(userPos);
            var marker = new google.maps.Marker({
                map: map,
                position: userPos,
            });
            var infowindow = new google.maps.InfoWindow({
                content: "Hakemasi osoite: " + address
            });
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, marker);
            });
            userMarkers.push(marker);
            calcDistance(userPos);
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
}

/**
 * Funktio joka asettaa KAIKKI ottomaatit kartalle (tehdään kun sivu avataan ensimmäisen kerran)
*/
function placeMarker(lat, long, osoite, pstnro, id, kaupunki, sijainti) {
    var latlon = new google.maps.LatLng(lat, long);
    var marker = new google.maps.Marker({
        position: latlon,
        id: id,
        icon: "ottoikoni.png",
        map: map,
        osoite: osoite,
        pstnro: pstnro,
        kaupunki: kaupunki,
        sijainti: sijainti,
        distance: null
    });
    setMap(map, marker);
    var infowindow = new google.maps.InfoWindow({
        content: '<p>' + sijainti + '</p>' + '<p>' + osoite + '<br>' + pstnro + ' ' + kaupunki + '</p>'
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
    });
    collection.push(marker);
    console.log(collection[index].position + " " + collection[index].osoite);
    index++;
}
